use grab_plugin::{
    Grabber,
    AssetCreator,
    AssetWriter,
    Type,
    Ident,
    TokenStream,
    quote,
    register_grabbers,
    ty,
};
use grab_plugin::failure::{Error, bail, ResultExt};
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
use utf8::BufReadDecoder;

struct FileGrabber;

#[derive(Deserialize, Hash)]
#[serde(transparent)]
struct File {
    path: PathBuf,
}

#[derive(Deserialize, Serialize)]
struct Metadata {
    id: String,
    path: PathBuf,
    check_utf8: bool,
}

impl Grabber for FileGrabber {
    type Input = File;
    type Metadata = Metadata;

    fn generate_sync<C: AssetCreator + ?Sized>(
        self,
        input: File,
        assets: &mut C,
        relative_to: &Path,
        grab_ty: Type,
        grab_host: Ident)
        -> Result<(TokenStream, Self::Metadata), Error>
    {
        let mut meta = Metadata {
            id: assets.create(),
            path: if input.path.is_relative() {
                let mut path = relative_to.to_owned();
                path.push(input.path);
                path
            } else {
                input.path
            },
            check_utf8: false,
        };
        let id = &meta.id;
        Ok(if grab_ty == ty!(Vec<u8>) {
            (quote! {
                let mut buffer = Vec::new();
                std::io::Read::read_to_end(#grab_host.load(#id), &mut buffer)?;
                buffer
            }, meta)
        } else if grab_ty == ty!(String) {
            meta.check_utf8 = true;
            (quote! {
                let mut buffer = String::new();
                std::io::Read::read_to_string(#grab_host.load(#id), &mut buffer)?;
                buffer
            }, meta)
        } else {
            bail!("can't read file into {:?}", grab_ty)
        })
    }
    
    fn process<W: AssetWriter + ?Sized>(
        self,
        meta: Metadata,
        assets: &mut W)
        -> Result<(), Error>
    {
        use std::io::{copy, BufReader};
        use std::fs::File;

        let open_ctx = format!("{} could not be opened", meta.path.display());
        if meta.check_utf8 {
            let mut read = BufReadDecoder::new(BufReader::new(
                File::open(&meta.path).context(open_ctx.clone())?
            ));
            while let Some(result) = read.next_strict() {
                match result {
                    Ok(_) => (),
                    Err(_) => bail!("file is not utf-8"),
                }
            }
        }

        assets.write(&meta.id, &mut |writer| {
            let mut file = File::open(&meta.path).context(open_ctx)?;
            copy(&mut file, writer)?;
            Ok(())
        })
    }
}

register_grabbers! {
    "file" => FileGrabber,
}
