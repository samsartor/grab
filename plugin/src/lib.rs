pub mod raw;

pub use raw::{AssetHost, AssetCreator, AssetWriter, Grabber};
pub use syn::{Type, Ident};
pub use proc_macro2::TokenStream;
pub use quote::quote;
pub use failure;

#[doc(hidden)]
pub use syn::parse_quote;

#[macro_export]
macro_rules! ty {
    ($($x:tt)*) => { {
        let x: $crate::Type = $crate::parse_quote! { $($x)* };
        x
    } };
}

#[macro_export]
macro_rules! register_host {
    ($($x:tt)*) => {
        #[no_mangle]
        pub extern "C" fn __grab_create_host()
            -> *mut $crate::raw::RawHost
        {
            fn make_host() -> impl $crate::AssetHost { $($x)* }

            Box::into_raw(Box::new($crate::raw::ReInit {
                id: (),
                init: make_host,
                instance: None,
            }))
        }
    };
}

#[macro_export]
macro_rules! register_grabbers {
    ($($n:expr => $x:expr),*$(,)?) => {
        #[no_mangle]
        pub extern "C" fn __grab_create_grabbers(len: *mut usize)
            -> *mut Box<dyn $crate::raw::RawGrabber> 
        {
            use std::mem::forget;

            let mut grabbers = vec![$({
                fn make_grabber() -> impl $crate::Grabber { $x }

                Box::new($crate::raw::ReInit {
                    id: String::from($n),
                    init: make_grabber,
                    instance: None,
                }) as Box<dyn $crate::raw::RawGrabber>
            }),*];

            unsafe { *len = grabbers.len(); }
            let ptr = grabbers.as_mut_ptr();
            forget(grabbers);
            ptr
        }
    };
}
