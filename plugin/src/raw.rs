use erased_serde::{Deserializer, Serializer};
use proc_macro2::TokenStream;
use failure::Error;
use std::path::Path;
use serde::{de::DeserializeOwned, Serialize};
use std::io::Write;
use syn::{Type, Ident};

pub type WithWrite<'a> = &'a mut dyn for<'w> FnOnce(&'w mut dyn Write)
    -> Result<(), Error>;

pub struct ReInit<I, T> {
    pub id: I,
    pub init: fn() -> T,
    pub instance: Option<T>,
}

impl<I, T> ReInit<I, T> {
    pub fn instance_mut(&mut self) -> &mut T {
        self.instance.get_or_insert_with(self.init)
    }

    pub fn take_instance(&mut self) -> T {
        self.instance.take().unwrap_or_else(self.init)
    }
}

pub trait RawHost: AssetCreator + AssetWriter {
    fn after_creations(&mut self) -> Result<(), Error>;
    fn after_writes(&mut self) -> Result<(), Error>;
    fn finish(&mut self) -> Result<(), Error>;
}

pub trait AssetCreator {
    fn create(&mut self) -> String;
}

pub trait AssetWriter {
    fn write(&mut self, id: &str, writer: WithWrite) -> Result<(), Error>;
}

pub trait AssetHost: AssetCreator + AssetWriter {
    fn after_creations(self) -> Result<(), Error>;
    fn after_writes(self) -> Result<(), Error>;
    fn finish(self) -> Result<(), Error>;
}

impl<H: AssetHost> AssetCreator for ReInit<(), H> {
    fn create(&mut self) -> String {
        self.instance_mut().create()
    }
}

impl<H: AssetHost> AssetWriter for ReInit<(), H> {
    fn write(&mut self, id: &str, writer: WithWrite) -> Result<(), Error> {
        self.instance_mut().write(id, writer)
    }
}

impl<H: AssetHost> RawHost for ReInit<(), H> {
    fn after_creations(&mut self) -> Result<(), Error> {
        self.take_instance().after_creations()
    }

    fn after_writes(&mut self) -> Result<(), Error> {
        self.take_instance().after_writes()
    }

    fn finish(&mut self) -> Result<(), Error> {
        self.take_instance().finish()
    }
}

pub trait RawGrabber {
    fn name(&self) -> &str;

    fn generate_sync(
        &mut self,
        input: &mut dyn Deserializer,
        meta: &mut dyn Serializer,
        assets: &mut dyn RawHost,
        relative_to: &Path,
        grab_ty: Type,
        grab_host: Ident)
        -> Result<TokenStream, Error>;
    
    fn process(
        &mut self,
        meta: &mut dyn Deserializer,
        assets: &mut dyn RawHost)
        -> Result<(), Error>;
}

pub trait Grabber {
    type Input: DeserializeOwned;
    type Metadata: Serialize + DeserializeOwned;

    fn generate_sync<C: AssetCreator + ?Sized>(
        self,
        input: Self::Input,
        assets: &mut C,
        relative_to: &Path,
        grab_ty: Type,
        grab_host: Ident)
        -> Result<(TokenStream, Self::Metadata), Error>;
    
    fn process<W: AssetWriter + ?Sized>(
        self,
        meta: Self::Metadata,
        assets: &mut W)
        -> Result<(), Error>;
}

impl<G: Grabber> RawGrabber for ReInit<String, G> {
    fn name(&self) -> &str {
        self.id.as_str()
    }

    fn generate_sync(
        &mut self,
        input: &mut dyn Deserializer,
        meta: &mut dyn Serializer,
        assets: &mut dyn RawHost,
        relative_to: &Path,
        grab_ty: Type,
        grab_host: Ident)
        -> Result<TokenStream, Error>
    {
        let input: G::Input = erased_serde::deserialize(input)?;
        let (stream, output) = self.take_instance().generate_sync(
            input,
            assets,
            relative_to,
            grab_ty,
            grab_host,
        )?;
        output.serialize(meta)?;
        Ok(stream)
    }

    fn process(
        &mut self,
        meta: &mut dyn Deserializer,
        assets: &mut dyn RawHost)
        -> Result<(), Error> 
    {
        let meta: G::Metadata = erased_serde::deserialize(meta)?;
        self.take_instance().process(
            meta,
            assets,
        )
    }
}
