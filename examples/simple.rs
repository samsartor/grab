use grab::{GrabSync, DirHost, grab_host};

#[derive(GrabSync)]
pub struct Poems {
    #[grabber = File("./poem.txt")]
    pub peanut: String;
}

fn main() {
    let dir_host = grab_host!(DirHost, "./assets/");
    let poems = Poems::grab(&mut dir_host).unwrap();
    println!("{}", poems.peanut);
}
