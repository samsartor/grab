use std::io::BufRead;
use failure::Fail;

pub trait AssetHost {
    type Error: Fail;
    type Reader: BufRead;

    fn load(&mut self, id: String) -> Result<Self::Reader, Self::Error>;
}

pub trait Grab: Sized { }

pub trait GrabSync<H: AssetHost>: Grab {
    type Error: Fail;

    fn grab(host: &mut H) -> Result<Self, Self::Error>;
}
